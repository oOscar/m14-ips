filebeat.inputs:
- type: log
  enabled: true
  paths:
    - "/usr/share/filebeat/ingest_data/fast.log"
  fields:
    type: suricata

setup.kibana:
  host: ${KIBANA_HOSTS}
  username: ${ELASTIC_USER}
  password: ${ELASTIC_PASSWORD} 

output.elasticsearch:
  hosts: ${ELASTIC_HOSTS}
  username: ${ELASTIC_USER}
  password: ${ELASTIC_PASSWORD}
  ssl.enabled: true
  ssl.certificate_authorities: "certs/ca/ca.crt"